# 1. Check Hostname
```
hostname
hostnamectl
```
## Hostname List
```
cat /etc/hosts
vi /etc/hosts
```
## Ganti Hostname
```
hostnamectl set-hostname <nama_hostname_baru>
nmtui

setelahnya reconect SSH
```
# 2. Check OS
## Check Versi OS
```
hostnamectl
cat /etc/system-release
cat /etc/os-release

```
## Check Versi OS Kernel
```
uname -r
cat /proc/version
```
# 3. Check Spesifikasi Hardware

## Check Spesifikasi CPU
```
cat /proc/cpuinfo

```
## Check Spesifikasi Memory & Swap
```
cat /proc/meminfo
free -m
free -g
```
# 4. Disk
## Check Type Disk
```
df -Th
```
## Check Kapasitas Disk Yang Terpakai
```
df -h
df -kh
```
## Create LV Baru
```
df -Th
vgs
lvcreate -n lv_data -L 98G simasvg
mkdir /data
mkfs.ext4 /dev/simasvg/lv_data
vi /etc/fstab

tambahkan line :
/dev/mapper/simasvg-lv_data /data                  ext4    defaults        1 2
lalu save

mount -a

```
## Extend Disk
```
lsblk 
vgs
vgextend simasvg /dev/sdb
lvextend -L +10G -r /dev/simasvg/lv_root
```
## Extend Swap
```
free -g
swapoff /dev/simasvg/swap
lvresize -L +13G /dev/simasvg/swap
mkswap /dev/simasvg/swap
swapon -a

```
# 5. Process Monitoring
## Process yang berjalan
```
ps -ef|grep <process>
ps -ef|grep java
```
## Process secara keseluruhan


# Belajar Perintah Sed (stream editor)
```
sed [option] commands [input-file]

pakai sed -i jika ingin benar benar input
```
## Menampilkan Baris Tertentu
```
Option -n berfungsi menampilkan baris yang diperintahkan saja.

sed -n '1,3p' sample.txt

[root@pleasechange ~]# sed -n '1,3p' sample.txt
Jawa Sumatra Jawa
Papua Sulawesi Bali
Jawa Kalimantan Sulawesi
[root@pleasechange ~]#


```

## Mengganti / Replace String

### cara pertama ini hanya untuk ganti kata pertama yang ditemukan pada awal baris 

```
sed 's/Jawa/Madura/' sample.txt

output :

[root@pleasechange ~]# sed 's/Jawa/Madura/' sample.txt
Madura Sumatra Jawa
Papua Sulawesi Bali
Madura Kalimantan Sulawesi
Sumatra Sulawesi Papua
Sulawesi Maluku Sulawesi
Madura Papua Jawa
Bali Madura Kalimantan
[root@pleasechange ~]#

Dari output diatas hanya kata jawa di baris pertama kata pertama saja yg menjadi madura , namun jawa diakhir tidak terganti 
```
### cara kedua ini hanya untuk ganti kata pada suatu baris sampai ujung baris
```
jika mau kata jawa sampai ujung kalimat yg diganti, pakai /g diakhirnya

sed 's/Jawa/Madura/g' sample.txt

[root@pleasechange ~]# sed 's/Jawa/Madura/g' sample.txt
Madura Sumatra Madura
Papua Sulawesi Bali
Madura Kalimantan Sulawesi
Sumatra Sulawesi Papua
Sulawesi Maluku Sulawesi
Madura Papua Madura
Bali Madura Kalimantan
[root@pleasechange ~]#

```

## Mengganti string (baris)

```
Mengganti kalimat baris kedua dengan kalimat cibinong

sed '2c\Cibinong' sample.txt

[root@pleasechange ~]# sed '2c\Cibinong' sample.txt
Jawa Sumatra Jawa
Cibinong
Jawa Kalimantan Sulawesi
Sumatra Sulawesi Papua
Sulawesi Maluku Sulawesi
Jawa Papua Jawa
Bali Jawa Kalimantan

```

## Menghapus String (baris)
```
menghapus berdasarkan baris

sed '2,4d' sample.txt

output

[root@pleasechange ~]# sed '2,4d' sample.txt
Jawa Sumatra Jawa
Sulawesi Maluku Sulawesi
Jawa Papua Jawa
Bali Jawa Kalimantan
[root@pleasechange ~]#

```

## Menghapus String (kata)
```
menghapus berdasarkan kata , akan menghapus baris yang mengandung kata kalimantan

[root@pleasechange ~]# sed '/Kalimantan/d' sample.txt
Jawa Sumatra Jawa
Papua Sulawesi Bali
Sumatra Sulawesi Papua
Sulawesi Maluku Sulawesi
Jawa Papua Jawa
[root@pleasechange ~]#

```

## Menysipkan baris baru
```
Menyisipkan kata indonesia di baris ketika , dengan perintah i

sed '3i\Indonesia' sample.txt

[root@pleasechange ~]# sed '3i\Indonesia' sample.txt
Jawa Sumatra Jawa
Papua Sulawesi Bali
Indonesia
Jawa Kalimantan Sulawesi
Sumatra Sulawesi Papua
Sulawesi Maluku Sulawesi
Jawa Papua Jawa
Bali Jawa Kalimantan
[root@pleasechange ~]#

```
